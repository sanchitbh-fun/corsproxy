'use strict';

var request = require('request');

module.exports.proxy = (event, context, callback) => {
  var reply = request(event.path.substr(1), function(err, response, body) {
      callback(null, { statusCode: 200, headers: {
        "Access-Control-Allow-Origin": "*",
		"Content-Type": "text/html"
      }, body: body.toString() });
  });
};
